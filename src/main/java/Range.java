import java.util.Scanner;

public class Range {

    public static int getRange(int[] arr){

        int minimum=Integer.MAX_VALUE, maximum= Integer.MIN_VALUE;

        for(int i:arr){
            if(minimum>i)
                minimum= i;
            if(maximum<i)
                maximum= i;
        }

        return maximum-minimum;

    }

    public static void main(String[] args) {

        Scanner sc= new Scanner(System.in);

        int size;
        size= sc.nextInt();

        int[] arr= new int[size];
        for(int i=0; i<size; i++){
            arr[i]= sc.nextInt();
        }

        int range= getRange(arr);
        System.out.print(range);

    }
}
